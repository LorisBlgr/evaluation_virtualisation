FROM php:8.2-cli

WORKDIR /evaluation_virtualisation

COPY src/ /evaluation_virtualisation/src/
COPY tests/ /evaluation_virtualisation/tests/
COPY composer.json composer.lock /evaluation_virtualisation/
COPY vendor/ /evaluation_virtualisation/vendor/

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install --no-interaction --no-progress --prefer-dist

EXPOSE 80

CMD ["php", "-S", "0.0.0.0:80", "-t", "src", "src/main.php"]
