<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../src/SphereVolume.php';

use PHPUnit\Framework\TestCase;

class SphereVolumeTest extends TestCase
{
    /**
     * @covers SphereVolume::calculateVolume
     */
    public function testCalculateVolumeWithNegativeValue()
    {
        $sphere = new SphereVolume(-5);
        $sphere->calculateVolume();
        $this->assertNull($sphere->getVolume(), "Volume should be null for negative radius");
    }

    /**
     * @covers SphereVolume::calculateVolume
     */
    public function testCalculateVolumeWithPositiveValue()
    {
        $sphere = new SphereVolume(5);
        $sphere->calculateVolume();
        $this->assertEquals((4 / 3) * pi() * pow(5, 3), $sphere->getVolume(), "Volume calculation is incorrect for positive radius");
    }

    /**
     * @covers SphereVolume::calculateVolume
     */
    public function testCalculateVolumeWithNullValue()
    {
        $sphere = new SphereVolume(null);
        $sphere->calculateVolume();
        $this->assertNull($sphere->getVolume(), "Volume should be null for null radius");
    }
}
