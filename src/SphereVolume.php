<?php
/**
 * Calculate the volume of a sphere with its radius
 */
class SphereVolume
{
    /** Radius */
    public $radius;
    /** Volume */
    public $volume;

    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    /**
     * Calculate the volume of a sphere
     * @return void
     */
    public function calculateVolume()
    {
        if ($this->radius > 0) {
            $this->volume = (4 * pi() * pow($this->radius, 3)) / 3;
        } else {
            $this->volume = null;
        }
    }

    /**
     * Get the volume of a sphere
     * @return float
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Save the volume in Volume.txt
     */
    public function saveVolume()
    {
        $myfile = fopen("Volume.txt", "w") or die("Error opening file!");
        fwrite($myfile, $this->volume);
        fclose($myfile);
    }

    /**
     * Get the volume from Volume.txt
     */
    public function getVolumeFromFile()
    {
        $this->volume = file_get_contents("Volume.txt");
        return $this->volume;
    }
}
