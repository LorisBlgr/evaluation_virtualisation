<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/SphereVolume.php';

if(isset($_POST['radius'])) {
    $radius = $_POST['radius'];

    $sphere = new SphereVolume($radius);

    // calculate the volume
    $sphere->calculateVolume();
    // save the volume in Volume.txt
    $sphere->saveVolume();
    // get the volume from Volume.txt
    $volume = $sphere->getVolumeFromFile();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://threejs.org/build/three.js"></script>
    <title>Document</title>
</head>
<body>
    <div style="text-align:center;">
        <h1>Volume d'une sphère par son rayon.</h1>
        <form method="post">
            <label for="radius">Saisir le rayon de la sphère</label>
            <input type="number" name="radius" min="1" required>
            <input type="submit">
        </form>
        <p>Le volume d'une sphère de rayon <?php if(isset($radius)) {
            echo $radius;
        } ?> est égale à <?php if(isset($volume)) {
            echo $volume;
        } ?></p>
    </div>
    <h2 style="text-align:center;">Représentation de la sphère avec threeJS</h2>
    <div class="canvas"></div>
    <script>
        // Create a scene
        let scene = new THREE.Scene();

        // Create a camera
        let camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        camera.position.z = 5;

        // Create a renderer
        let renderer = new THREE.WebGLRenderer();
        renderer.setSize(window.innerWidth, window.innerHeight);
        document.querySelector('.canvas').appendChild(renderer.domElement);

        // get the radius from the volume
        let radius = <?= $radius; ?>;

        // Create a sphere geometry with the calculated radius
        let geometry = new THREE.SphereGeometry(radius, 32, 32);
        let material = new THREE.MeshBasicMaterial({color: 0x00ff00});
        let sphere = new THREE.Mesh(geometry, material);
        scene.add(sphere);

        // draw lines on the sphere
        let edges = new THREE.EdgesGeometry(geometry);
        let line = new THREE.LineSegments(edges, new THREE.LineBasicMaterial({color: 0xffffff}));
        sphere.add(line);

        // Animation loop
        function animate() {
            requestAnimationFrame(animate);
            renderer.render(scene, camera);
        }
        animate();
    </script>
</body>
</html>
